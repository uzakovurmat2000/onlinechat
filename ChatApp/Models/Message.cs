﻿using System;
using System.Collections.Generic;

namespace ChatApp.Models;

public partial class Message
{
    public string Id { get; set; } = null!;

    public string? Text { get; set; }

    public DateTime? Timestamp { get; set; }

    public string? FromUser { get; set; }

    public string? ToUser { get; set; }

    public bool IsResponse { get; set; }

    public virtual User? FromUserNavigation { get; set; }

    public virtual User? ToUserNavigation { get; set; }
}

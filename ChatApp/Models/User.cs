﻿using System;
using System.Collections.Generic;

namespace ChatApp.Models;

public partial class User
{
    public string Id { get; set; } = null!;
    public string? OrgName { get; set; }
    public List<string>? Subject { get; set; }

    public string? UserName { get; set; }

    public string? Email { get; set; }

    public string? Password {  get; set; }

    public string? Role { get; set; }

    public string? isDeleted { get; set; }

    public virtual ICollection<Message> MessageFromUserNavigations { get; set; } = new List<Message>();

    public virtual ICollection<Message> MessageToUserNavigations { get; set; } = new List<Message>();
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;


namespace ChatApp.Hubs
{
    public class ChatHub : Hub
    {
        private readonly ChatContext _chatContext;
        public static Dictionary<string, string> UserNameConnectionId = new();

        public ChatHub(ChatContext chatContext)
        {   
            _chatContext = chatContext;
        }

        public async Task<string> GetConnectionId(string usermail) 
        {
            var user = await _chatContext.Users.SingleOrDefaultAsync(x => x.Email == usermail);


            if (user != null)
            {
                if (UserNameConnectionId.ContainsKey(user.Email))
                {
                    UserNameConnectionId[user.Email] = Context.ConnectionId;
                }
                else
                {
                    UserNameConnectionId.Add(user.Email, Context.ConnectionId);
                }
                return Context.ConnectionId;
            }
            else 
            {
                throw new KeyNotFoundException($"User with username '{usermail}' not found.");
            }
           
        }

    }
}

﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build(),
    connectionId;
//Disable send button until connection is established
$("#sendButton").disabled = true;



connection.start().then(async function () {
    console.log("start connection func");
    $("#sendButton").prop("disabled", false);
    console.log("send btn disabled");
    var username = $('#myUsername').val();
    console.log("username" + username);
    try {
        connectionId = await connection.invoke('getConnectionId', username);
        console.log("connection ID: " + connectionId);
    } catch (error) {
        console.error("invoke error: " + error.toString());
    }

}).catch(function (err) {
    return console.error("connection failed " + err.toString());
});

connection.on("RecieveMessage", function (message, time) {
    $('.chat-wrapper.shown .chat').append('<div class="bubble other"><span class="message-text">' + message + ' </span>'
        + '<span class="message-time">' + time + '</span></div>');
    scrollToBottom();
})

var sendMessage = function (recipient) {
    console.log("sendMessage function called");
    event.preventDefault();

    var $text = $("#message-send").val();
    console.log("----------------   message text: " + $text);
    $.ajax({
        type: 'POST',
        url: '/Chat/SendMessage',
        data: { to: recipient, text: $text },
        cache: false,
        success: function () {
            $(".write textarea").val('');
            $(".chat-wrapper.shown .chat").append('<div class="bubble me"><span class="message-text">' + $text + ' </span>'
                + '<span class="message-time">' + getTimeNow() + '</span></div>');
            scrollToBottom();
        },
        error: function (xhr, status, error) {
            console.error("Failed to send message!", status, error);
            console.error(xhr.responseText); // Добавьте эту строку
            alert("Failed to send message! Check the console for details.");
        }
    });
}




function getTimeNow() {
    return new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
}

function scrollToBottom() {
    $(".chat-wrapper.shown .chat").animate({ scrollTop: $('.chat-wrapper.shown .chat').prop("scrollHeight") }, 500);
}


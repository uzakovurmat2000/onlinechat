﻿using ChatApp.Hubs;
using ChatApp.Models;
using Humanizer;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Npgsql.Replication.PgOutput.Messages;
using System.Security.Claims;

namespace ChatApp.Controllers
{
    public class ChatController : Controller
    {
        private readonly ChatContext chatContext;
        private readonly CustomUserManager _userManager;
        private readonly IHubContext<ChatHub> _hubcontext;
        private readonly ILogger<ChatController> _logger;
        public ChatController(ChatContext data, CustomUserManager userManager, IHubContext<ChatHub> hubContext, ILogger<ChatController> logger)
        {
            chatContext = data;
            _userManager = userManager;
            _hubcontext = hubContext;
            _logger = logger;
        }
        [HttpPost]
        public async Task<IActionResult> SendMessage(string to, string text)         
        {                        
            try
            {
                var sender = await _userManager.GetUserAsync(HttpContext.User);
                if (sender.Role != "2")
                {
                    return Forbid();
                }
                var recipient = await chatContext.Users.SingleOrDefaultAsync(x => x.Email == to);

                Message message = new Message()
                {
                    Id = Guid.NewGuid().ToString(),

                    FromUser = sender.Id,
                    ToUser = recipient.Id,
                    Text = text,
                    Timestamp = DateTime.UtcNow,
                    IsResponse = false
                };
                await chatContext.AddAsync(message);
                await chatContext.SaveChangesAsync();

                string connectionId = ChatHub.UserNameConnectionId[recipient.Email];

                await _hubcontext.Clients.Client(connectionId).SendAsync("RecieveMessage", message.Text, message.Timestamp?.ToString("HH:mm"));
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while sending message.");
                return StatusCode(500, "Internal Server Error");
            }

           
        }
        [HttpPost]
        public async Task<IActionResult> SendResponse(string to, string text, string messageId)
        {
            try
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);

                var originalMessage = await chatContext.Messages.FindAsync(messageId);

                /* var recipient = await chatContext.Users.Where(x => x.Role == "2" && x.Subject.Intersect(user.Subject).Any()).ToListAsync();
                 var randomRecipient = recipient.OrderBy(x => Guid.NewGuid()).FirstOrDefault();*/
                if (user.Role != "1") 
                {
                    return Forbid("You do not have permission to send responses.");
                }


                Message response = new Message()
                {
                    Id = Guid.NewGuid().ToString(),

                    FromUser = user.Id,
                    ToUser = originalMessage.Id,
                    Text = text,
                    Timestamp = DateTime.UtcNow,
                    IsResponse = true
                };
                await chatContext.AddAsync(response);
                await chatContext.SaveChangesAsync();

                string connectionId = ChatHub.UserNameConnectionId[originalMessage.FromUser];

                await _hubcontext.Clients.Client(connectionId).SendAsync("RecieveMessage", response.Text, response.Timestamp?.ToString("HH:mm"));
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while sending message.");
                return StatusCode(500, "Internal Server Error");
            }
        }


            public string GetCurrentUserName()
        {
            ClaimsPrincipal user = HttpContext.User as ClaimsPrincipal;

            if (user != null)
            {
                Claim usernameClaim = user.FindFirst(ClaimTypes.Email);
                return usernameClaim?.Value;
            }

            return null;
        }


        public string GetCurrentUserId()
        {
            ClaimsPrincipal user = HttpContext.User as ClaimsPrincipal;
            if (user != null)
            {
                Claim userIdClaim = user.FindFirst(ClaimTypes.NameIdentifier);
                return userIdClaim?.Value;
            }

            return null;
        }
    }
}

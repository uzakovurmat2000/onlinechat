﻿using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace ChatApp.Models
{
    public class CustomUserManager
    {

        private readonly ChatContext _chatContext;

        public CustomUserManager(ChatContext chatContext)
        {
            _chatContext = chatContext;
        }

        public async Task<User> GetUserAsync(ClaimsPrincipal user) 
        {
            var userName = user.Identity.Name;
            var foundUser = await _chatContext.Users.SingleOrDefaultAsync(u => u.UserName == userName);
            return foundUser;
          
        }

    }
}

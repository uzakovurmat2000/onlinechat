﻿using ChatApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Security.Claims;

namespace ChatApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ChatContext _chatcontext;
        private readonly User _user;
        public HomeController(ILogger<HomeController> logger , ChatContext _context)
        {
            _logger = logger;
            _chatcontext = _context;
        }
        
            public async Task<IActionResult> Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }   
            var curUser = GetCurrentUserId();
            ViewBag.currentuser = GetCurrentUserName();
            var user = await _chatcontext.Users.FindAsync(curUser);
            var allMessages = await _chatcontext.Messages.Where(x =>
                x.FromUser == user.Id ||
                x.ToUser == user.Id)
                .ToListAsync();

            var chats = new List<ChatViewModel>();
            foreach (var i in await _chatcontext.Users.ToListAsync())
            {
                if (i == user) continue;

                var chat = new ChatViewModel()
                {
                    MyMessages = allMessages.Where(x => x.FromUser == user.Id && x.ToUser == i.Id).ToList(),
                    OtherMessages = allMessages.Where(x => x.FromUser == i.Id && x.ToUser == user.Id).ToList(),
                    RecipientEmail = i.Email
                };

                var chatMessages = new List<Message>();
                chatMessages.AddRange(chat.MyMessages);
                chatMessages.AddRange(chat.OtherMessages);

                chat.LastMessage = chatMessages.OrderByDescending(x => x.Timestamp).FirstOrDefault();

                chats.Add(chat);
            }

            return View(chats);
        }
        public string GetCurrentUserId()
        {
            ClaimsPrincipal user = HttpContext.User as ClaimsPrincipal;
            if (user != null)
            {
                Claim userClaim = user.FindFirst(ClaimTypes.NameIdentifier);
                return userClaim?.Value;
            }

            return null;
        }
        public string GetCurrentUserName()
        {
            ClaimsPrincipal user = HttpContext.User as ClaimsPrincipal;
            if (user != null)
            {
                Claim userClaim = user.FindFirst(ClaimTypes.Email);
                return userClaim?.Value;
            }

            return null;
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using ChatApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace ChatApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly ChatContext _chatContext;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AccountController(ChatContext chatContext, IHttpContextAccessor httpContextAccessor)
        {
            _chatContext = chatContext;
             _httpContextAccessor = httpContextAccessor;
        }


        public IActionResult LoginAdmin()
        {
            return View();
        }
         
        [HttpPost]
        public async Task<ActionResult> LoginAdminAsync(User model) 
        {
            var user = _chatContext.Users.FirstOrDefault(u => u.UserName == model.UserName);
            if (user != null)
            {
                string hashedPasswordFromDatabase = user.Password;
                if (BCrypt.Net.BCrypt.Verify(model.Password, hashedPasswordFromDatabase))
                {
                    var role = user.Role;
                   
                    if (!string.IsNullOrEmpty(role))
                    {
                        var claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.NameIdentifier, user.Id),
                            new Claim(ClaimTypes.Name, user.UserName),
                            new Claim(ClaimTypes.Email, user.Email),
                            new Claim(ClaimTypes.Role, user.Role)

                        };
                        var claimsIdentity = new ClaimsIdentity(claims, "AuthScheme");

                        await _httpContextAccessor.HttpContext
                            .SignInAsync("AuthScheme",
                                new ClaimsPrincipal(claimsIdentity),
                                new AuthenticationProperties());

                  

                        return RedirectToAction("Index", "Home");

                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "У пользователя нет роли");
                    }

                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Неправильное имя пользователя или пароль.");
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Неправильное имя пользователя или пароль.");
            }
            return View(model);
        }


        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> LoginAsync(User model)
        {
            var user = await _chatContext.Users.SingleOrDefaultAsync(u => u.Email == model.Email);

            if (user != null)
            {               
                user.Subject = model.Subject;
                _chatContext.SaveChangesAsync();
                await AuthenticateUserAsync(user);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                user = new User
                {
                    Id = Guid.NewGuid().ToString(),
                    Email = model.Email,
                    UserName = model.UserName,
                    OrgName = model.OrgName,
                    Subject = model.Subject,
                    Role = "2",
                    isDeleted = "0"
                };
                try
                {
                    _chatContext.Users.Add(user);
                    await _chatContext.SaveChangesAsync();
                    await AuthenticateUserAsync(user);
                    ViewBag.SuccessMessage = "Операция добавления выполнена успешно";
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", ex.ToString());
                    return View();
                }

            }
            
        }
        
        private async Task AuthenticateUserAsync(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, user.Role)
            };

            var claimsIdentity = new ClaimsIdentity(claims, "AuthScheme");

            await _httpContextAccessor.HttpContext.SignInAsync("AuthScheme",
                new ClaimsPrincipal(claimsIdentity),
                new AuthenticationProperties());
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(User model)
        {
            if (_chatContext.Users.Any(u => u.UserName == model.UserName))
            {
                ModelState.AddModelError("Email", "Пользователь уже существует");
                return View();
            }

            string salt = BCrypt.Net.BCrypt.GenerateSalt();
            var passw = model.Password;
    
            var user = new User
            {
                Id = Guid.NewGuid().ToString(),
                Email = model.Email,
                UserName = model.UserName,
                Password = BCrypt.Net.BCrypt.HashPassword(passw, salt),
                Role = "1"
            };
            try
            {
                var result = await _chatContext.Users.AddAsync(user);


                try
                {
                    await _chatContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error!!!!!", ex.ToString());
                    return View();
                }

                if (result.State == EntityState.Added)
                {
                    ViewBag.SuccessMessage = "Операция добавления выполнена успешно";
                    return View(user);
                }
                else
                {
                    ModelState.AddModelError("Error", "Ошибка при добавлении пользователя");
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Error", ex.ToString());
                return View();
            }
            return View(user);
        }



        [HttpPost]
        public async Task<ActionResult> LogoutAsync()
        {
            await HttpContext.SignOutAsync("AuthScheme");

            return RedirectToAction("Login", "Account");

        }


    }


}

